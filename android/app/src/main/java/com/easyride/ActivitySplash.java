package com.easyride;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.LinearLayout;


public class ActivitySplash extends Activity implements OnCancelListener
{
    private class IntroAnimationListener implements AnimationListener
    {
        private Activity		m_Parent	= null;
        private LinearLayout	m_Layout	= null;
        private int 			m_nStep		= 0;


        public IntroAnimationListener (Activity parent, int step)
        {
            m_Parent	= parent;
            m_nStep		= step;
            m_Layout	= (LinearLayout) findViewById (R.id.introAnimation);
        }

        @Override
        public void onAnimationEnd (Animation animation)
        {
            AlphaAnimation localAlphaAnimation = null;

            switch (m_nStep)
            {
                case 0 :
                    localAlphaAnimation = new AlphaAnimation (0.0F, 1.0F);
                    localAlphaAnimation.setDuration		(STEP_TIME);
                    localAlphaAnimation.setFillAfter	(true);

                    m_Layout.setAnimation (localAlphaAnimation);
                    localAlphaAnimation.setAnimationListener (this);
                    break;

                case 1 :
                    localAlphaAnimation = new AlphaAnimation (1.0F, 0.0F);
                    localAlphaAnimation.setDuration		(STEP_TIME);
                    localAlphaAnimation.setFillAfter	(true);

                    m_Layout.setAnimation (localAlphaAnimation);
                    localAlphaAnimation.setAnimationListener (this);
                    break;

                case 2 :
                    m_Layout.setBackgroundResource (R.drawable.intro_second);

                    localAlphaAnimation = new AlphaAnimation (0.0F, 1.0F);
                    localAlphaAnimation.setDuration		(STEP_TIME);
                    localAlphaAnimation.setFillAfter	(true);

                    m_Layout.setAnimation (localAlphaAnimation);
                    localAlphaAnimation.setAnimationListener (this);
                    break;

                case 3 :
                    localAlphaAnimation = new AlphaAnimation (1.0F, 0.0F);
                    localAlphaAnimation.setDuration		(STEP_TIME);
                    localAlphaAnimation.setFillAfter	(true);

                    m_Layout.setAnimation (localAlphaAnimation);
                    localAlphaAnimation.setAnimationListener (this);
                    break;

                default :
                    m_Parent.startActivity (new Intent (ActivitySplash.this, TabbedActivity.class));
                    m_Parent.finish ();
                    break;
            }

            m_nStep ++;
        }

        @Override
        public void onAnimationRepeat (Animation animation) { }

        @Override
        public void onAnimationStart (Animation animation) { }
    }




    private static final long		STEP_TIME		= 1600;

    private MediaPlayer				m_MediaPlayer	= null;
    private IntroAnimationListener	m_Listener		= null;




    @Override
    public void onCreate(Bundle bundle)
    {
        super.onCreate (bundle);

        setContentView (R.layout.intro);

        m_Listener = new IntroAnimationListener (this, 0);
        m_Listener.onAnimationEnd (null);

        m_MediaPlayer = MediaPlayer.create (getBaseContext (), R.raw.accel);
        m_MediaPlayer.start ();
    }

    @Override
    public void onCancel (DialogInterface dialog)
    {
        if (m_MediaPlayer != null)
        {
            m_MediaPlayer.release ();
            m_MediaPlayer = null;
        }

        finish ();
    }

    @Override
    public void onStop ()
    {
        super.onStop ();

        if (m_MediaPlayer != null)
        {
            m_MediaPlayer.release ();
            m_MediaPlayer = null;
        }

        finish ();
    }
}