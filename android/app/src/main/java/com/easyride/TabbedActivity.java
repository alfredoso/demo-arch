package com.easyride;

import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;




public class TabbedActivity extends TabActivity
{
    private TabHost m_TabHost = null;

    @Override
    public void onCreate (Bundle bundle)
    {
        super.onCreate (bundle);

        setContentView (R.layout.custom_menu_bar);

        m_TabHost = getTabHost ();

        setupViews ();

        m_TabHost.setCurrentTab (0);
    }


    public void switchTab(int tab){
        m_TabHost.setCurrentTab(tab);
    }


    private static View createTabView (Context context, String title, int iconId)
    {
        View		view			= null;
        TextView	titleTextView	= null;
        ImageView	iconImageView	= null;


        view			= LayoutInflater.from (context).inflate (R.layout.custom_tab, null);
        titleTextView	= (TextView)	view.findViewById (R.id.CustomTabText);
        iconImageView	= (ImageView)	view.findViewById (R.id.CustomTabIcon);

        titleTextView.setText			(title);
        iconImageView.setImageResource	(iconId);

        return view;
    }

    private void addActivityTab (int titleId, int iconId, Intent intent)
    {
        View	tabView	= null;
        String	title	= null;
        TabSpec	tabSpec	= null;


        title	= (String) getText (titleId);
        tabView	= createTabView (m_TabHost.getContext (), title, iconId);
        tabSpec	= m_TabHost.newTabSpec (title);

        tabSpec.setIndicator	(tabView);
        tabSpec.setContent		(intent);
        m_TabHost.addTab (tabSpec);
    }

    private void setupViews ()
    {
        addActivityTab (R.string.menu_native, R.drawable.button_record,	new Intent (this, NativeGaugeActivity.class));
        addActivityTab (R.string.menu_react, R.drawable.button_upload,	new Intent (this, MainActivity.class));

    }
}